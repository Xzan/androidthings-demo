package be.xzan.blinkingled.common

import android.util.Log
import com.google.firebase.database.*

class FirebaseObserver(private val listener: OnLedChangedListener) : ValueEventListener {

    companion object {
        const val LED = "led"
        const val BLINKING = "blinking"
        const val INTERVAL = "interval"
    }

    private val ref: DatabaseReference by lazy { FirebaseDatabase.getInstance().getReference(LED) }

    fun start() {
        ref.addValueEventListener(this)
    }

    fun stop() {
        ref.removeEventListener(this)
    }

    fun setBlinking(blinking: Boolean) {
        ref.child(BLINKING).setValue(blinking)
    }

    fun setInterval(interval: Long) {
        ref.child(INTERVAL).setValue(interval)
    }

    override fun onDataChange(data: DataSnapshot?) {
        Log.d("TAGG", "Data changed")
        if (data != null) {
            listener.onLedChanged(
                    Led(
                            data.child(BLINKING).getValue(Boolean::class.java) ?: false,
                            data.child(INTERVAL).getValue(Long::class.java) ?: 1000
                    )
            )
        }
    }

    override fun onCancelled(data: DatabaseError?) {

    }

    data class Led(val blinking: Boolean, val interval: Long)

    interface OnLedChangedListener {

        fun onLedChanged(led: Led)
    }

}