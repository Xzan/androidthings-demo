package be.xzan.blinkingled

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.View
import android.widget.Button
import android.widget.SeekBar
import android.widget.TextView
import be.xzan.blinkingled.common.FirebaseObserver

class MainActivity : AppCompatActivity(), FirebaseObserver.OnLedChangedListener, View.OnClickListener, SeekBar.OnSeekBarChangeListener {

    val observer : FirebaseObserver by lazy { FirebaseObserver(this) }
    val seekbar : SeekBar by lazy { findViewById<SeekBar>(R.id.sb_interval) }
    val button : Button by lazy { findViewById<Button>(R.id.bt_toggle) }
    val text : TextView by lazy { findViewById<TextView>(R.id.tv_toggle) }
    var led : FirebaseObserver.Led? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.main_activity)
        observer.start()
        button.setOnClickListener(this)
        seekbar.setOnSeekBarChangeListener(this)
    }

    override fun onDestroy() {
        observer.stop()
        super.onDestroy()
    }

    override fun onClick(p0: View?) {
        led?.let {
            observer.setBlinking(!it.blinking)
        }
    }

    override fun onProgressChanged(p0: SeekBar?, p1: Int, p2: Boolean) {

    }

    override fun onStartTrackingTouch(p0: SeekBar?) {

    }

    override fun onStopTrackingTouch(p0: SeekBar?) {
        observer.setInterval(seekbar.progress.toLong())
    }

    override fun onLedChanged(led: FirebaseObserver.Led) {
        this.led = led
        text.setText(if (led.blinking) { R.string.led_on } else { R.string.led_off })
        seekbar.setOnSeekBarChangeListener(null)
        seekbar.progress = led.interval.toInt()
        seekbar.setOnSeekBarChangeListener(this)
    }
}