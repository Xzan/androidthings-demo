# Introduction
Ce projet est un test afin de tester [Android Things](https://developer.android.com/things/hardware/raspberrypi.html) (DP 1).
Le but de l'exercice est d'obtenir une led clignotante que l'on peut arrêter ou allumer à l'aide
d'un bouton.

Le projet est exclusivement réalisé sur un [Raspberry Pi 3](https://developer.android.com/things/hardware/raspberrypi.html) mais peut être aisément porté sur les
autres cartes supportées par Android Things.

# Installation
Suivez les étapes d'installation d'Android Things sur le Raspberry Pi 3 que vous
pouvez trouver à l'adresse suivante : https://developer.android.com/things/hardware/raspberrypi.html

Connectez les éléments électroniques sur le breadboard de la façon suivante :

 ![](./images/connections.png)

# Rappel
Les connecteurs du Raspberry Pi sont les suivants :

![connecteurs](https://developer.android.com/things/images/pinout-raspberrypi.png)

# Dernière étape
Il ne vous reste plus qu'à lancer le code et le mettre sur le Raspberry Pi