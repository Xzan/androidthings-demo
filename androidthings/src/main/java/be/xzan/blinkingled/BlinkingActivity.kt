package be.xzan.blinkingled

import android.app.Activity
import android.os.Bundle
import android.os.Handler
import android.os.Message
import android.util.Log
import be.xzan.blinkingled.common.FirebaseObserver

import com.google.android.things.pio.Gpio
import com.google.android.things.pio.GpioCallback
import com.google.android.things.pio.PeripheralManagerService

import java.io.IOException
import java.lang.ref.WeakReference


class BlinkingActivity : Activity(), FirebaseObserver.OnLedChangedListener {

    companion object {
        const val TAG = "BlinkActivity"
        const val INTERVAL_BETWEEN_BLINKS_MS = 1000L
        const val GPIO_LED_NAME = "BCM6"
        const val GPIO_BUTTON_NAME = "BCM5"
    }

    private val blinkingLedHandler = BlinkingHandler(this)
    private val service = PeripheralManagerService()
    private val ledGpio: Gpio by lazy { service.openGpio(GPIO_LED_NAME) }
    private val buttonGpio: Gpio by lazy { service.openGpio(GPIO_BUTTON_NAME) }
    private val observer : FirebaseObserver by lazy { FirebaseObserver(this) }
    private var led : FirebaseObserver.Led? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        try {
            Log.d(TAG, "onCreate: ")
            ledGpio.setDirection(Gpio.DIRECTION_OUT_INITIALLY_LOW)
            buttonGpio.setDirection(Gpio.DIRECTION_IN)
            buttonGpio.setEdgeTriggerType(Gpio.EDGE_FALLING)
            buttonGpio.registerGpioCallback(object : GpioCallback() {
                override fun onGpioEdge(gpio: Gpio?): Boolean {
                    onButtonPressed()
                    return true
                }
            })
            observer.start()
        } catch (e: IOException) {
            throw RuntimeException("Problem connecting to IO Port", e)
        }
    }

    override fun onLedChanged(led: FirebaseObserver.Led) {
        blinkingLedHandler.removeMessages(0)
        this.led = led
        if (led.blinking) {
            invertLed()
        } else {
            ledGpio.value = false
        }
    }

    private fun onButtonPressed() {
        Log.i(TAG, "GPIO changed, button pressed")
        led?.let { observer.setBlinking(!it.blinking) }
    }

    private fun invertLed() {
        try {
            ledGpio.value = !ledGpio.value
            blinkingLedHandler.sendEmptyMessageDelayed(0, led?.interval ?: INTERVAL_BETWEEN_BLINKS_MS)
        } catch (e: IOException) {
            Log.e(TAG, "Error on PeripheralIO API", e)
        }
    }

    override fun onDestroy() {
        try {
            blinkingLedHandler.removeMessages(0)
            buttonGpio.close()
            ledGpio.close()
            observer.stop()
        } catch (e: IOException) {
            Log.e(TAG, "Error on PeripheralIO API", e)
        }
        super.onDestroy()
    }

    private class BlinkingHandler internal constructor(activity: BlinkingActivity) : Handler() {

        private val reference = WeakReference(activity)

        override fun handleMessage(msg: Message) {
            val activity = reference.get()
            activity?.invertLed()
        }
    }
}
